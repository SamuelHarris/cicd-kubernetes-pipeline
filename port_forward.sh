#!/bin/bash

NAMESPACE="jenkins"

for i in $(kubectl get pods -n $NAMESPACE --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
do
    if [[ $i == "jenkins"* ]]; then
        kubectl -n $NAMESPACE port-forward $i 8080 &
    elif [[ $i == "registry"* ]]; then
        kubectl -n $NAMESPACE port-forward $i 5000 &
    fi 
done